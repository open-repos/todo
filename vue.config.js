module.exports = {

    pluginOptions: {
        webpackBundleAnalyzer: {
            openAnalyzer: false
        }
    },

    css: {
        loaderOptions: {
            sass: {
                prependData: `@import "@/assets/scss/main.scss";`
            }
        }
    },

    publicPath: process.env.NODE_ENV === 'production' ? '/todo/' : '/',

};