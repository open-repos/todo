function timeConverter( unix_timestamp ){
    let date_str = new Date(unix_timestamp);

    let hour = date_str.getHours();
    let min = date_str.getMinutes();
    min = parseInt(min) < 10 ? '0' + min : min
    let sec = date_str.getSeconds();
    
    let time = hour + ':' + min;
    return time;
}

function dateConverter( unix_timestamp, variant = 1 ){

    let date_str = new Date(unix_timestamp);

    let months_en = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    let months_ru = ['Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря'];

    let get_month = parseInt(date_str.getMonth())

    let year = parseInt(date_str.getFullYear());
    let month = get_month + 1;
    month = month < 10 ? '0' + month : month;
    let date = date_str.getDate() < 10 ? '0' + date_str.getDate() : date_str.getDate();

    let time;

    switch (variant) {
        case 1:
            time = date + '.' + month + '.' + year;
            break;

        case 2:
            time = date + ' ' + months_ru[get_month] + ' ' + year;
            break;
    
        default:
            time = date + '.' + month + '.' + year;
            break;
    }

    return time;
}

function shortText( text, length, suff = '...' ) {

    if ( text.length >= length ) {

        let substring = null

        if ( /\S/.test(text[length]) ) {
            for (let i = length + 1; i < (length + 21); i++) {
                
                if ( /\s/.test(text[i]) ) {
                    substring = text.substring(0, i)
                    break
                }

                if ( i === (length + 20) ) {
                    substring = text.substring(0, length)
                }
            }
        }
        else {
            substring = text.substring(0, length)
        }

        return substring + suff
    }
    else {
        return text
    }
}

module.exports = {
    timeConverter,
    dateConverter,
    shortText,
}