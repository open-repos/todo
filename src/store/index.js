import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersist from 'vuex-persist'

const vuexPersist = new VuexPersist({
	key: 'wcst-tasker-app',
	storage: window.localStorage
})

import modal from './modal'
import notes from './notes'

Vue.use(Vuex)

export default new Vuex.Store({

	state: {},

	mutations: {},

	actions: {},

	getters: {},


	modules: {
		modal: modal,
		notes: notes,
	},

	plugins: [vuexPersist.plugin]
})
