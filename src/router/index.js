import Vue from 'vue'
import VueRouter from 'vue-router'
import index from '../views/index.vue'

Vue.use(VueRouter)

const routes = [
	{
		path: '/',
		name: 'Home',
		component: index
	},
	{
		path: '/add-note',
		name: 'Add note',
		component: () => import('../views/add_note.vue')
	},
	{
		path: '/note/:time',
		name: 'Show note',
		component: () => import('../views/show_note.vue')
	},
	{
		path: '/note/:time/edit',
		name: 'Edit note',
		component: () => import('../views/edit_note.vue')
	},
]

const router = new VueRouter({
	mode: 'history',
	base: process.env.BASE_URL,
	routes
})

export default router
